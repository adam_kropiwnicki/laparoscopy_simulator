#include <opencv2/opencv.hpp>
#include "frame_stream.h"
#include <unistd.h>
#include <fstream>
using namespace cv;

const float calib_square = 0.015f; //meters
const Size chessboard_dim = Size(6,9);
const int camera_id = 2;
const int frames_per_s = 20;
const std::string names[2] = { "camera_1", "camera_2"};

void getOwnChessboardCorners(std::vector<Point3f>& corners) {
  for (int i=0; i<chessboard_dim.height; i++) {
    for (int j=0; j<chessboard_dim.width; j++) {
      corners.push_back(Point3f(j * calib_square, i * calib_square, 0.0f));
    }
  }
}

void getWorldSpaceChessboardCorners(std::vector<Mat> images, std::vector<std::vector<Point2f> >& all_corners, bool verbose=false) {
  for (std::vector<Mat>::iterator iter = images.begin(); iter != images.end(); iter++) {
    std::vector<Point2f> buffer;
    bool found = findChessboardCorners(*iter, chessboard_dim, buffer, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);
    if (found)
      all_corners.push_back(buffer);
    if (verbose) {
      drawChessboardCorners(*iter, chessboard_dim, buffer, found);
      imshow("Looking for chessboard corners:", *iter);
      waitKey(0);
    }
  }
}

void calibrate(std::vector<Mat> images, Mat& camera_matrix, Mat& dist_coef) {
  Size img_size = images[0].size();
  std::vector<std::vector<Point2f> > world_corners;
  getWorldSpaceChessboardCorners(images, world_corners);

  std::vector<std::vector<Point3f> > own_corners(1);
  getOwnChessboardCorners(own_corners[0]);
  own_corners.resize(world_corners.size(), own_corners[0]);

  std::vector<Mat> r_vecs, t_vecs;
  camera_matrix = initCameraMatrix2D(own_corners,world_corners,img_size,0);
  dist_coef = Mat::zeros(8, 1, CV_64F);
  calibrateCamera(own_corners, world_corners, chessboard_dim, camera_matrix, dist_coef, r_vecs, t_vecs);
}

bool saveParams(std::string name, Mat camera_matrix, Mat dist_coef) {
  std::ofstream outStream(name);
  if (outStream) {
    for (int r=0; r<camera_matrix.rows; r++) {
      for (int c=0; c<camera_matrix.cols; c++) {
	outStream << camera_matrix.at<double>(r,c) << std::endl;
      }
    }
    for (int r=0; r<dist_coef.rows; r++) {
      for (int c=0; c<dist_coef.cols; c++) {
	outStream << dist_coef.at<double>(r,c) << std::endl;
      }
    }
    outStream.close();
    std::cout<<"saved under name:  "<<name<<std::endl;
    return true;
  }
  return false;
}

int main() {

  FrameStream fs(camera_id);
  if (fs.init_capturing())
    return 1;
  sleep(3);

  bool found = false, calibrated = false;
  std::vector<Mat> images;
  std::vector<Vec2f> found_points;
  Mat frame, drawTo_frame, camera_mat, dist_coef;
  int count = 0;
  
  std::cout<<"Press SPACE to take snap, ENTER to start calibration on snaps, Anything other to exit\n";
  
  while (!calibrated) {
    fs.getFrame(frame);
    found = findChessboardCorners(frame, chessboard_dim, found_points, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);
    frame.copyTo(drawTo_frame);
    drawChessboardCorners(drawTo_frame, chessboard_dim, found_points, found);
    if (found) {
      imshow("Webcam", drawTo_frame);
      //    std::cout<<found_points[0]<<std::endl;
    }
    else
      imshow("Webcam", frame);
    char c = waitKey(1000 / frames_per_s);
    switch(c) {
    case 13:
      if (images.size() > 15) {
	calibrate(images, camera_mat, dist_coef);
	calibrated = true;
	saveParams(names[camera_id-1],camera_mat,dist_coef);
      } else {
	std::cout<<"More images required to start calibration!\n";
      }
      break;
    case ' ':
      if (found) {
	Mat tmp;
	frame.copyTo(tmp);
	images.push_back(tmp);
	std::cout<<++count<<std::endl;
      }
      break;
    case 27:
      calibrated = true;
      break;
    default:
      break;
    }
  }
  
  destroyAllWindows();
  fs.stop_capturing();
  
  return 0;
}
