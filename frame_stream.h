#ifndef FRAMESTREAM_H
#define FRAMESTREAM_H

#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <unistd.h>
using namespace cv;

class FrameStream {
  static void * captureFrame(void* arg) {
    FrameStream * fs = (FrameStream*)arg;
    VideoCapture cam(fs->camera_id);
    if(!cam.isOpened())
      pthread_exit((void*)-1);
    while(true) {
      pthread_mutex_lock (&fs->frameMutex);
      cam >> fs->frame;
      pthread_mutex_unlock (&fs->frameMutex);
      pthread_testcancel();
      usleep(6000);
    }
  }
public:
  pthread_mutex_t frameMutex;
  pthread_t stream_thread;
  Mat frame;
  int camera_id;
  FrameStream() {
    camera_id = 0;
  }
  FrameStream(int i) {
    camera_id = i;
  }
  int init_capturing() {
    int j; 
    j = pthread_mutex_init(&frameMutex, NULL);
    if (j) {
      std::cerr<<"Whoops - cannot initialze a mutex"<<std::endl;
      return -2;
    }
    j = pthread_create(&stream_thread, NULL, captureFrame, (void*)this);
    if (j) {
      std::cerr<<"Whoops - cannot create a capture thread"<<std::endl;
      return -1;
    }
    return 0;
  }
  
  void stop_capturing() {
      pthread_cancel(stream_thread);
      pthread_mutex_destroy(&frameMutex);
  }
  void getFrame(Mat & out) {
    pthread_mutex_lock(&frameMutex);
    frame.copyTo(out);
    pthread_mutex_unlock(&frameMutex);
  }
};
#endif
