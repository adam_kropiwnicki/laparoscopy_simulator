C++/OpenCV aplication for extracting position of tools viewed by 2 cameras
==========================================================================
***
***
* This repository is a part of science project made by student from Wroclaw Univeristy of Science and Technology. The goal was to create a dual-camera vision system for 3D reconstruction in a given workspace, in order to simulate laparoskopy operation. Such simulators usually work with high-end, expensive sensors as opposed to our cheap webcam solution.
* Linux is our targeted architecture, but you can try it anywhere with POSIX thread support.
***
## Repository includes (for now):
1. Single camera calibration program (calib.cpp) for finding intrinsic and extrinsic camera parameters using 6x9 chessboard calibration pattern
2. Stero-set calibration program (stereo-calib.cpp) for finding positional relation between cameras (also using 6x9 chessboard pattern)
3. main.cpp program displaying position of "laparoskopy tools"
4. Seperate version of main, called main_tracking.cpp which use object tracking algorithms - works poorly for now
5. frame_stream.h file containing FrameStream class for parallel frae acquisition (with POSIX threads)
6. CMakeList file for quick building with cmake
***
## Instalation steps:
1. Download and install [OpenCV Library](https://github.com/opencv/opencv) --- follow official opencv guidelines
	1. If you want to run main_trackin.cpp (object tracking algorithms), download and install also [Additional modules](https://github.com/opencv/opencv_contrib)
2. Clone this repository
3. Plug in your cameras 
4. Go to build directory and try to run main.
	1. In case of any error try to run 'make' in build directory
		1. In case of Makefile error try to run `cmake ..` to rebuild the project and rerun Makefile - you might have to download cmake first though (`sudo apt-get install cmake` on debian kernels)
5. If errors persists, make sure program does see your cameras. See next section for some info on that matter. 
***
## Important tips
Program is looking for cameras via so called camera_id -> when camera is being plugged in, unique id in form of next available number (counting from 0) is given to a camera. This number can be seen by listing contents of /dev directory. Be careful --- plug external cameras after system has booted, in order you want them to be in. According to opencv documentation this number can also refer to your USB port number.
Since our laptop had built-in camera, our programs by default search for camera with id 1 and 2. This can be changed:

* in calib.cpp just change camera_id global variable 
* in stereo_calib.cpp, main.cpp change the parameter with which FrameStream instance is initialized. In main.cpp it is in line 132
I'm using 6x9 chessboard calibration pattern with size of each tile 1,5 x 1,5 cm. If you have diffrent chessboard pattern set apropriate global variables in calib.cpp and stereo_calib.cpp files
### Future releases shall include interface to change all above parameters
***
## Contact
Email me at [235623@student.pwr.edu.pl](235623@student.pwr.edu.pl)