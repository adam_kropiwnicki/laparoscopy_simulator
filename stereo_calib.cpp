#include <opencv2/opencv.hpp>
#include "frame_stream.h"
#include <unistd.h>
#include <fstream>
using namespace cv;

#define NUM_CAMS 2

const float calib_square = 0.015f; //meters
const Size chessboard_dim = Size(6,9);
const int frames_per_s = 20;
const std::string names[2] = { "camera_1", "camera_2"};

void getOwnChessboardCorners(std::vector<Point3f>& corners) {
  for (int i=0; i<chessboard_dim.height; i++) {
    for (int j=0; j<chessboard_dim.width; j++) {
      corners.push_back(Point3f(j * calib_square, i * calib_square, 0.0f));
    }
  }
}

void getWorldSpaceChessboardCorners(std::vector<Mat> images, std::vector<std::vector<Point2f> >& all_corners, bool verbose=false) {
  for (std::vector<Mat>::iterator iter = images.begin(); iter != images.end(); iter++) {
    std::vector<Point2f> buffer;
    bool found = findChessboardCorners(*iter, chessboard_dim, buffer, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);
    if (found) 
      all_corners.push_back(buffer);
    if (verbose) {
      drawChessboardCorners(*iter, chessboard_dim, buffer, found);
      imshow("Looking for chessboard corners:", *iter);
      waitKey(0);
    }
  }
}

bool loadParams(std::string name, Mat & camera_matrix, Mat & dist_coef) {
  camera_matrix = Mat(3,3,CV_64F);
  dist_coef = Mat::zeros(5, 1, CV_64F);
  std::ifstream inStream(name);
  if (inStream) {
    for (int r=0; r<camera_matrix.rows; r++) {
      for (int c=0; c<camera_matrix.cols; c++) {
	inStream >> camera_matrix.at<double>(r,c);
	//std::cout<<camera_matrix.at<double>(r,c)<<std::endl;
      }
    }
    for (int r=0; r<dist_coef.rows; r++) {
      for (int c=0; c<dist_coef.cols; c++) {
	inStream >> dist_coef.at<double>(r,c);
	//std::cout<<dist_coef.at<double>(r,c)<<std::endl;
      }
    }
    inStream.close();
    return true;
  }
  return false;
}

void calibrate(std::vector<Mat> images_1, std::vector<Mat> images_2, Mat & rotation, Mat & translation, Mat & essential, Mat & fundamental) {
  Size img_size = images_1[0].size();
  std::vector<std::vector<Point2f> > world_corners[NUM_CAMS];
  getWorldSpaceChessboardCorners(images_1, world_corners[0]);
  getWorldSpaceChessboardCorners(images_2, world_corners[1]);
  
  std::vector<std::vector<Point3f> > own_corners(1);
  getOwnChessboardCorners(own_corners[0]);
  own_corners.resize(world_corners[0].size(), own_corners[0]);

  Mat camera_mat_1, camera_mat_2, dist_coef_1, dist_coef_2;
  if (!loadParams(names[0], camera_mat_1, dist_coef_1)) {
    std::cout<<"Unable to load params!\n";
    return;
  }
  if (!loadParams(names[1], camera_mat_2, dist_coef_2)) {
    std::cout<<"Unable to load params!\n";
    return;
  }

  stereoCalibrate(own_corners, world_corners[0], world_corners[1], camera_mat_1, dist_coef_1, camera_mat_2, dist_coef_2, img_size, rotation, translation, essential, fundamental);
}

bool saveParams(Mat rotation, Mat translation, Mat essential, Mat fundamental) {
  std::ofstream outStream("stereo_params");
  if (outStream) {
    for (int r=0; r<rotation.rows; r++) {
      for (int c=0; c<rotation.cols; c++) {
	outStream << rotation.at<double>(r,c) << std::endl;
      }
    }
    for (int r=0; r<translation.rows; r++) {
      for (int c=0; c<translation.cols; c++) {
	outStream << translation.at<double>(r,c) << std::endl;
      }
    }
    for (int r=0; r<essential.rows; r++) {
      for (int c=0; c<essential.cols; c++) {
	outStream << essential.at<double>(r,c) << std::endl;
      }
    }
    for (int r=0; r<fundamental.rows; r++) {
      for (int c=0; c<fundamental.cols; c++) {
	outStream << fundamental.at<double>(r,c) << std::endl;
      }
    }
    outStream.close();
    std::cout<<"saved under name:  stereo_params"<<std::endl;
    return true;
  }
  return false;
}

int main() {

  FrameStream fs[NUM_CAMS];
  for (int i=0;i<NUM_CAMS;i++) {
    fs[i] = FrameStream(i+1); 
      if (fs[i].init_capturing())
	return 1;
  }
  sleep(5);

  bool found[NUM_CAMS] = {false, false};
  bool calibrated = false;
  std::vector<Mat> images[NUM_CAMS];
  std::vector<Vec2f> found_points[NUM_CAMS];
  Mat frame[NUM_CAMS];
  Mat drawTo_frame[NUM_CAMS];
  Mat rotation, translation, essential, fundamental;
  int count = 0;
  
  std::cout<<"Press SPACE to take snap, ENTER to start calibration on snaps, Anything other to exit\n";
  
  while (!calibrated) {
    for (int i=0;i<NUM_CAMS;i++)
      fs[i].getFrame(frame[i]);
    for (int i=0;i<NUM_CAMS;i++) {
      found[i] = findChessboardCorners(frame[i], chessboard_dim, found_points[i], CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);
      frame[i].copyTo(drawTo_frame[i]);
      drawChessboardCorners(drawTo_frame[i], chessboard_dim, found_points[i], found[i]);
      if (found[i])
	imshow(names[i], drawTo_frame[i]);
      else
	imshow(names[i], frame[i]);
    }
    char c = waitKey(1000 / frames_per_s);
    switch(c) {
    case 13:
      if (images[0].size() > 15) {
	calibrate(images[0],images[1],rotation,translation,essential,fundamental);
	saveParams(rotation,translation,essential,fundamental);
	calibrated = true;
      } else {
	std::cout<<"More images required to start calibration!\n";
      }
      break;
    case ' ':
      if (found[0] && found[1]) {
	for (int i=0;i<NUM_CAMS;i++) {
	  Mat tmp;
	  frame[i].copyTo(tmp);
	  images[i].push_back(tmp);
	}
	std::cout<<++count<<std::endl;
      }
      break;
    case 27:
      calibrated = true;
      break;
    default:
      break;
    }
  }
  
  destroyAllWindows();
  for (int i=0;i<NUM_CAMS;i++) {
    fs[i].stop_capturing();
  }
  
  return 0;
}
