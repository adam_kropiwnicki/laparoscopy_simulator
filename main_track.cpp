#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <iostream>
#include <unistd.h>
#include "frame_stream.h"

#define NUM_CAMS 2
#define NUM_TOOLS 2

using namespace cv;
const std::string name[NUM_CAMS] = { "camera_1", "camera_2"};
Mat camera_matrix[NUM_CAMS];
Mat dist_coef[NUM_CAMS];

bool loadCameraMatrixes() {
  for (int i=0;i<NUM_CAMS;i++) {
    camera_matrix[i] = Mat(3,3,CV_64F);
    dist_coef[i] = Mat::zeros(5, 1, CV_64F);
    std::ifstream inStream(name[i]);
    if (inStream) {
      for (int r=0; r<camera_matrix[i].rows; r++) {
	for (int c=0; c<camera_matrix[i].cols; c++) {
	  inStream >> camera_matrix[i].at<double>(r,c);
	  //std::cout<<camera_matrix[i].at<double>(r,c)<<std::endl;
	}
      }
      for (int r=0; r<dist_coef[i].rows; r++) {
	for (int c=0; c<dist_coef[i].cols; c++) {
	  inStream >> dist_coef[i].at<double>(r,c);
	  //std::cout<<dist_coef[i].at<double>(r,c)<<std::endl;
	}
      }
      inStream.close();
    }
    else {
      std::cout<<"Couldn't open input stream to read file 'camera_"<<i<<"'!\n";
      return false;
    }
  }
  return true;
}
bool loadRTMatrix( Mat & R, Mat & T) {
  R = Mat(3,3,CV_64F);
  T = Mat::zeros(3, 1, CV_64F);
  std::ifstream inStream("stereo_params");
  if (inStream) {
    for (int r=0; r<R.rows; r++) {
      for (int c=0; c<R.cols; c++) {
	inStream >> R.at<double>(r,c);
	//std::cout<<camera_matrix.at<double>(r,c)<<std::endl;
      }
    }
    for (int r=0; r<T.rows; r++) {
      for (int c=0; c<T.cols; c++) {
	inStream >> T.at<double>(r,c);
	//std::cout<<dist_coef.at<double>(r,c)<<std::endl;
      }
    }
    inStream.close();
    return true;
  }
  else {
    std::cout<<"Couldn't open input stream to read file 'stereo_params'!\n";
    return false;
  }
}

void redFilter(Mat & in, Mat & out) {
  Mat mask1,mask2,image_HSV;
  out = Mat(in.rows,in.cols,in.type(),Scalar(0, 0, 0));
  cvtColor(in, image_HSV, COLOR_BGR2HSV);
  inRange(image_HSV, Scalar(0, 128, 128), Scalar(10, 255, 255), mask1);
  inRange(image_HSV, Scalar(160, 128, 128), Scalar(180, 255, 255), mask2);
  mask1 = mask1 + mask2;
  Mat kernel = Mat::ones(3,3, CV_32F);
  morphologyEx(mask1,mask1,cv::MORPH_OPEN,kernel);
  morphologyEx(mask1,mask1,cv::MORPH_CLOSE,kernel);
  bitwise_and(in,in,out,mask1);
  //out = mask1;
}
void greenFilter(Mat & in, Mat & out) {
  Mat mask1,image_HSV;
  out = Mat(in.rows,in.cols,in.type(),Scalar(0, 0, 0));
  cvtColor(in, image_HSV, COLOR_BGR2HSV);
  inRange(image_HSV, Scalar(40, 128, 128), Scalar(70, 255, 255), mask1);
  Mat kernel = Mat::ones(3,3, CV_32F);
  morphologyEx(mask1,mask1,cv::MORPH_OPEN,kernel);
  morphologyEx(mask1,mask1,cv::MORPH_CLOSE,kernel);
  bitwise_and(in,in,out,mask1);
  //out = mask1;
}
void filterRedGreen(Mat & in, Mat & out) {
  Mat mask1,mask2,mask3,image_HSV;
  out = Mat(in.rows,in.cols,in.type(),Scalar(0, 0, 0));
  cvtColor(in, image_HSV, COLOR_BGR2HSV);
  inRange(image_HSV, Scalar(0, 128, 128), Scalar(10, 255, 255), mask1);
  inRange(image_HSV, Scalar(160, 128, 128), Scalar(180, 255, 255), mask2);
  inRange(image_HSV, Scalar(40, 128, 128), Scalar(85, 255, 255), mask3);
  mask1 = mask1 + mask2 + mask3;
  Mat kernel = Mat::ones(3,3, CV_32F);
  morphologyEx(mask1,mask1,cv::MORPH_OPEN,kernel);
  morphologyEx(mask1,mask1,cv::MORPH_CLOSE,kernel);
  bitwise_and(in,in,out,mask1);
  //out = mask1;
}

void cannyFilter(Mat & in, Mat & out) {
  cvtColor(in, out, CV_BGR2GRAY);
  GaussianBlur(out, out, Size(7,7), 1.5, 1.5);
  Canny(out, out, 0, 30, 3);
}
void printOutput(Mat & o) {
  char axes[3] = {'x','y','z'};
  std::cout<<"----------------------------------------\n";
  std::cout<<"Tool 1 (red):   ";
  for (int i=0;i<o.rows-1;i++) {
    std::cout<<axes[i]<<" = "<<o.at<double>(i,0)/o.at<double>(3,0)<<", ";
  }
  std::cout<<std::endl;
  std::cout<<"Tool 2 (green):   ";
  for (int i=0;i<o.rows-1;i++) {
    std::cout<<axes[i]<<" = "<<o.at<double>(i,1)/o.at<double>(3,1)<<", ";
  }
  std::cout<<std::endl;
}

int main() {
  MultiTracker tracker[NUM_CAMS];
  std::vector<Rect2d> bboxes[NUM_CAMS];
  Mat R,T,R1,R2,P1,P2,Q;
  Mat out;
  if(!loadCameraMatrixes())
    return 3;
  if(!loadRTMatrix(R,T))
    return 4;
  FrameStream fs[NUM_CAMS];
  for (int i=0;i<NUM_CAMS;i++) {
    fs[i] = FrameStream(i+1); 
      if (fs[i].init_capturing())
	return 1;
  }
  sleep(5);
  Size img_size = fs[0].frame.size();
  stereoRectify(camera_matrix[0],dist_coef[0],camera_matrix[1],dist_coef[1],img_size,R,T,R1,R2,P1,P2,Q);
  Mat res;
  for (int i=0; i<NUM_CAMS;i++) {
    filterRedGreen(fs[i].frame,res);
    for (int j=0; j<NUM_TOOLS; j++) {
      bboxes[i].push_back(selectROI("tracker",res));
      if(bboxes[i].size()<j+1) {
	std::cerr << "You had to draw bounding box on this frame!\n";
	return 2;
      }
      tracker[i].add(TrackerKCF::create(),res,bboxes[i][j]);
      // MIL, KCF, TLD, Boosting, MedianFlow,MOSSE,CSRT
    }
  }
  destroyAllWindows();
  int count = 0;
  std::vector<Point2d> tools_pos[NUM_CAMS];
  
  namedWindow(name[0]);
  namedWindow(name[1]);
  
  while(waitKey(1) < 0) {
    for (int i=0; i<NUM_CAMS;i++) {
      filterRedGreen(fs[i].frame,res);
      //res = fs[i].frame;
      tracker[i].update(res);
      for(unsigned j=0;j<tracker[i].getObjects().size();j++) {
	rectangle(res, tracker[i].getObjects()[j], Scalar( 255, 0, 0 ), 2, 1 );
	tools_pos[i].push_back(Point2d(tracker[i].getObjects()[j].x + (tracker[i].getObjects()[j].width / 2), tracker[i].getObjects()[j].y + (tracker[i].getObjects()[j].height / 2)));
      }
      imshow(name[i],res);
    }
    triangulatePoints(P1,P2,tools_pos[0],tools_pos[1],out);
    count++;
    if (count == 10) {
      system("clear");
      printOutput(out);
      count = 0;
    }
    for (int i=0;i<NUM_CAMS;i++)
      tools_pos[i].clear();
  }
  destroyAllWindows();
  
  for (int i=0;i<NUM_CAMS;i++) {
    fs[i].stop_capturing();
  }
  return 0;
}
